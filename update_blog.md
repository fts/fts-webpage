# How to add a blog entry

* Setup develop/test
  * mount https://test-fts.web.cern.ch/_site/files/themes/fts-webpage
  * cd to the mountpoint
  * git status # check we're in sync. Should be branch develop
* Edit
  * edit `page.tpl.php` and `news.html`with the same info. You can remove an older entry from `page.tpl.php` to make room.
  * copy an image to `./assets/img/news`
  * flush the server-side cache : https://test-fts.web.cern.ch/admin/config/development/performance
  * Check content at https://test-fts.web.cern.ch/
  * If it's OK, commit and push your changes, including the image
* Setup master/live
  * mount https://fts.web.cern.ch/_site/files/themes/fts-webpage
  * cd to the mountpoint
  * git status # should be up to date on master
  * git merge develop
  * flush the server-side cache - https://fts.web.cern.ch/admin/config/development/performance
  * Check content at https://fts.web.cern.ch/
  * Push changes
* Unmount DAV volumes

