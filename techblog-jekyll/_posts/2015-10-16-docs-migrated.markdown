---
layout:     post
title:      "Documentation migrated to Git + Markdown + GitBook"
subtitle:   ""
date:       2015-10-16 12:00:00
author:     "Alejandro Alvarez Ayllon"
tags:       [documentation]

---
<p>We have migrated out documentation to a <a href="https://gitlab.cern.ch/fts/documentation">Git repository</a>, from which online HTML pages are generated using <a href="https://www.gitbook.com/">GitBook</a>&nbsp;automatically every night.</p>

<p>Keeping the documentation in Git allows us to keep history, and tag the documentation together with the software they document. We believe this helps to keep track of how the service has changed with time. Also, documentation for older versions will remain easily reachable in case someone needs them.</p>

<p>Here is the link to the new docs:&nbsp;http://fts3-docs.web.cern.ch/fts3-docs/</p>

<p>Pull requests are welcome! :)</p>
