---
layout:     post
title:      "First Functional Prototype Of WebFTS"
subtitle:   ""
date:       2013-05-12 12:00:00
author:     "Alejandro Alvarez Ayllon"
header-img: "img/post-bg-01.jpg"
tags:       [webfts, demo, prototype]

---

<p>Today Andres has presented the first working prototype of WebFTS. This web interface allows user to easily interact with FTS3 without the need for special command line clients. Only a web browser is needed.</p>

<p>This is a extremely early stage prototype, but it even includes the possibility for listing source and destination directories, which relies on the small subset of data management capabilities of our RESTful API.</p>

<p>Looks promising!</p>


<a href="#">
    <img src="{{ site.baseurl | prepend: site.url }}/img/webfts-born.png" alt="">
</a>
