---
layout:     post
title:      "Easy To Run GridFTP Server"
subtitle:   ""
date:       2017-09-28 12:00:00
author:     "Alejandro Alvarez Ayllon"
tags:       [gridftp, docker]

---
<p>While FTS is mainly used to transfer data within the <a href="http://wlcg.web.cern.ch/">WLCG</a>, it can also be used, and it is, to transfer data from an experiment towards the data center.</p>

<p>A sucessful example of this use case is&nbsp;<a href="https://na62.web.cern.ch/na62/">NA62</a>. They use a decicated service for the Data Acquisition (DAQ). That way, they can rely on FTS to do the scheduling, optimize the transfers, retry, etc. Not only this: they can also benefit of the ecosystem surrounding the service, specially monitoring and archiving.</p>

<p>However, while the service is run by the IT department, an using it is relatively easy, the experiment does need to run some server software so FTS can access the files. The default choice tends to be <a href="https://en.wikipedia.org/wiki/GridFTP">GridFTP</a>, since it is well known and supported on the Grid.</p>

<p>To work, GridFTP requires a certificate to be installed on the storage. For experiments witihin CERN, it isn't hard to obtain one, but for other experiments external this may not be that trivial.</p>

<p>To make things easier, we provide now a Docker container with GridFTP preinstalled and preconfigured, bundled with <a href="https://letsencrypt.org/">Let's Encrypt</a>&nbsp;certbot client. In this manner, on boot time the container will automatically obtain a certificate, and start the GridFTP server, ready to be used by an existing FTS instance.</p>

<p>Some conmfiguration is still required, of course, as opening the necessary ports on the firewall. But, overall, the procedure becomes simpler.</p>

<p>To give it a try, just checkout our <a href="https://gitlab.cern.ch/fts/gridftp">repository</a>, and run docker-compose up. The documentation on the repository will give some hints on how to open ports on your machine if the firewall is up. Depending on your network you may need to ask those ports to be open to the outside as well.</p>

<p>&nbsp;</p>

