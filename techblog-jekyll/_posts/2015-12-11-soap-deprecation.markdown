---
layout:     post
title:      "SOAP Deprecation"
subtitle:   ""
date:       2015-12-11 12:00:00
author:     "Alejandro Alvarez Ayllon"
tags:       [soap]

---
<p>We would like to announce our intent to <a href="https://its.cern.ch/jira/browse/FTS-284">deprecate</a> the FTS3 SOAP API.</p>

<p>The SOAP API was initially in the project mainly to keep backwards compatibility towards the gLite FTS clients, and for quite some time they haven't been supported anymore.</p>

<p>The reasons for this deprecation are:</p>

<ul>
	<li>It is hard to extend. Any new features may require even the addition of new calls, depending on the original WSDL definitions.</li>
	<li>It runs on the FTS3 core process, which also schedules and runs the transfers. Bugs and inestabilities in a user-facing code can potentially cause a crash on the service.</li>
	<li>Experiments that have migrated fully to FTS3, and using its new features, have done so talking directly to the REST API, so it is a waste of resources to keep two entry points.</li>
</ul>

<p>&nbsp;</p>

<p>We will, of course, keep support for the command line tools,&nbsp;<strong>except the configuration ones</strong>. They are already partially able to talk REST, and they will become fully REST compliant soon. Additionally, to make things easier, it won't be even be necessary to manually use the switch "--rest". The CLI will be able to figure it out by itself.</p>

<h2><span>Stages</span></h2>

<ol>
	<li><span>Beginning of 1Q 2016</span>

	<ul>
		<li>Fully implement all the functionality for fts-transfer-* and fts-delegation-init in REST (<a href="https://its.cern.ch/jira/browse/FTS-286">FTS-286</a>), and remove the need for "--rest"</li>
		<li>Print a warning when SOAP is used, but keep working</li>
		<li>Print a deprecation warning for the SOAP Python bindings (<a href="https://its.cern.ch/jira/browse/FTS-285">FTS-285</a>, DONE)</li>
		<li>Document alternatives for the fts-config family (<a href="https://its.cern.ch/jira/browse/FTS-387">FTS-387</a>, DONE)</li>
	</ul>
	</li>
	<li>Beginning of 3Q 2016
	<ul>
		<li>Remove SOAP support from the new clients</li>
		<li>Remove the SOAP Python bindings</li>
	</ul>
	</li>
	<li>Q1 2017
	<ul>
		<li>Remove SOAP support from the server side</li>
	</ul>
	</li>
</ol>

<p>Please, mind the timing is just a proposal and can be moved.</p>

<h2>Actions to be taken by the users and experiments</h2>

<ol>
	<li>If you are using the REST API directly, you don't need to worry</li>
	<li>If you are using the CLI, as soon as FTS-286 is released, switch from port :8443 to :8446 (the CLI will remind you to do so)</li>
	<li>If you are using the gLite clients, move to the FTS3 clients or use the REST interface directly</li>
</ol>

<h2>Actions to be taken by system administrators and users with configuration rights</h2>

<p>We intend to stop supporting fts-config and family, since they are seldom used, and only by a handful of people.</p>

<p>Everything you can do with them, you can already achieve using directly REST. We have an <a href="http://fts3-docs.web.cern.ch/fts3-docs/fts-rest/docs/config_alternatives.html">equivalence guide between the config CLI and the REST API</a>.</p>

<p>Do not hesitate to let us know if you miss something in the documentation, or if the lack of configuration CLI can cause a big trouble.</p>

<p>&nbsp;</p>
