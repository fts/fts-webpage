var wow=new WOW({mobile:false});wow.init();
$(document).ready(function(){$('.wpb-mobile-menu').slicknav({prependTo:'.navbar-header',parentTag:'liner',allowParentLinks:true,duplicate:true,label:'',closedSymbol:'<i class="fa fa-angle-right"></i>',openedSymbol:'<i class="fa fa-angle-down"></i>',});});
$('#clients-scroller').owlCarousel({items:4,itemsTablet:3,margin:90,stagePadding:90,smartSpeed:450,itemsDesktop:[1199,4],itemsDesktopSmall:[980,3],itemsTablet:[768,3],itemsTablet:[767,2],itemsTabletSmall:[480,2],itemsMobile:[479,1],});
$('#color-client-scroller').owlCarousel({items:4,itemsTablet:3,margin:90,stagePadding:90,smartSpeed:450,itemsDesktop:[1199,4],itemsDesktopSmall:[980,3],itemsTablet:[768,3],itemsTablet:[767,2],itemsTabletSmall:[480,2],itemsMobile:[479,1],});
$('#testimonial-item').owlCarousel({autoPlay:5000,items:3,itemsTablet:3,margin:90,stagePadding:90,smartSpeed:450,itemsDesktop:[1199,4],itemsDesktopSmall:[980,3],itemsTablet:[768,3],itemsTablet:[767,2],itemsTabletSmall:[480,2],itemsMobile:[479,1],});
$('#testimonial-dark').owlCarousel({autoPlay:5000,items:3,itemsTablet:3,margin:90,stagePadding:90,smartSpeed:450,itemsDesktop:[1199,4],itemsDesktopSmall:[980,3],itemsTablet:[768,3],itemsTablet:[767,2],itemsTabletSmall:[480,2],itemsMobile:[479,1],});
$('#single-testimonial-item').owlCarousel({singleItem:true,autoPlay:5000,items:1,itemsTablet:1,margin:90,stagePadding:90,smartSpeed:450,itemsDesktop:[1199,4],itemsDesktopSmall:[980,3],itemsTablet:[768,3],itemsTablet:[767,2],itemsTabletSmall:[480,2],itemsMobile:[479,1],stopOnHover:true,});
$("#image-carousel").owlCarousel({autoPlay:3000,items:4,itemsDesktop:[1170,3],itemsDesktopSmall:[1170,3]});
$("#carousel-image-slider").owlCarousel({navigation:false,slideSpeed:300,paginationSpeed:400,singleItem:true,pagination:false,autoPlay:3000,});

$(document).ready(function() { 
	//checks difference between number of rows and ids. If none, guide is complete and code can be removed.
	//if a result is used in more that one question reduce the value or results by the number of reuses
	var rows = $('#qTable tr').length - 1; 
	var liids = $('#qTable li').length;
	if(rows   != liids) {	  
	  $('#errdiv').html('Number of rows ( ' + rows + ' ) does not match the number of questions ( ' +liids + ' )')
	}

  $('#qTable li').on('click',function() {
    //style the selected answer
    $(this).addClass('selectedAnswer').siblings().removeClass('selectedAnswer');								
	//find the id of the first question in the graoup								
	//var q1id = $(this).parent().children('li:first').find('i').text();
	//hide all rows after the currently displayed row and remove selectedAnswer style
	var rowCurrent = $(this).closest("tr").prevAll("tr").length + 2; 
	var rowsAfter = ' tr:nth-child(n+' + rowCurrent + ')';
	$('#qTable' + rowsAfter).hide().find('li').removeClass('selectedAnswer');
	//show the next row that matches the question id
	var italNum =  $(this).find('i').text();
	var qNext = ' tr:nth-child(' + italNum + ')'; 
	$('#qTable' + qNext).fadeIn(800);
	//scroll code to bring next question into view
	var qNextPos = $('#qTable' + qNext).offset();
	var qNextTop = qNextPos.top;
	var qNextHigh = $('#qTable' + qNext).height();
	var qNextBot = qNextHigh + qNextTop + 20; 
	var scrHigh = $(window).innerHeight();
	var difHigh = qNextBot - scrHigh; 
	if(difHigh > 0) {window.scrollTo(0, difHigh)}
	})
})
$(document).ready(function(){$('#carousel-about-us').owlCarousel({navigation:true,navigationText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],slideSpeed:800,paginationSpeed:400,autoPlay:true,singleItem:true,pagination:false,items:1,itemsCustom:false,itemsDesktop:[1199,4],itemsDesktopSmall:[980,3],itemsTablet:[768,2],itemsTabletSmall:false,itemsMobile:[479,1],});});
$(function(){$('#portfolio').mixItUp();});$('testimonial-carousel').carousel();$('a[data-slide="prev"]').click(function(){$('#testimonial-carousel').carousel('prev');});
$('a[data-slide="next"]').click(function(){$('#testimonial-carousel').carousel('next');});jQuery(document).ready(function($){$('.counter').counterUp({delay:1,time:1100});});
$('.skill-shortcode').appear(function(){$('.progress').each(function(){$('.progress-bar').css('width',function(){return($(this).attr('data-percentage')+'%')});});},{accY:-100});var offset=200;var duration=500;$(window).scroll(function(){if($(this).scrollTop()>offset){$('.back-to-top').fadeIn(400);}else{$('.back-to-top').fadeOut(400);}});$('.back-to-top').click(function(event){event.preventDefault();
$('html, body').animate({scrollTop:0},600);return false;})