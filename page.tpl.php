<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <!-- Viewport Meta Tag -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="File Transfer Service - Shipping Data around the World">
    <meta name="author" content="Maria Arsuaga Rios">
    <meta name="keywords" content="FTS, CERN, Storage, Transfer, CERN analysis">
    <title>
      FTS: File Transfer Service
    </title>
     <?php
      global $base_url;   // Will point to http://www.example.com
      $link = $base_url . '/' . path_to_theme();
      ?>
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="<?php print $link . '/assets/css/bootstrap.min.css' ?>">
    <!-- Main Style -->
    <link rel="stylesheet" type="text/css" href="<?php print $link . '/assets/css/main.css' ?>">
    <!-- Slicknav Css -->
    <link rel="stylesheet" type="text/css" href="<?php print $link . '/assets/css/slicknav.css' ?>">
    <!-- Responsive Style -->
    <link rel="stylesheet" type="text/css" href="<?php print $link . '/assets/css/responsive.css' ?>">
    <!--Fonts-->
    <link rel="stylesheet" media="screen" href="<?php print $link . '/assets/fonts/font-awesome/font-awesome.min.css' ?>">
    <link rel="stylesheet" media="screen" href="<?php print $link . '/assets/fonts/simple-line-icons.css' ?>">
    <!-- Extras -->
    <link rel="stylesheet" type="text/css" href="<?php print $link . '/assets/extras/owl/owl.carousel.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?php print $link . '/assets/extras/owl/owl.theme.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?php print $link . '/assets/extras/animate.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?php print $link . '/assets/extras/normalize.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?php print $link . '/assets/extras/settings.css' ?>">

    <!-- Color CSS Styles  -->
    <link rel="stylesheet" type="text/css" href="<?php print $link . '/assets/css/colors/green.css' ?>" media="screen" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js">
    </script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js">
    </script>
    <![endif]-->
  </head>
  <body>

    <!-- Header area wrapper starts -->
     <header id="header-wrap">
       <!-- Navbar Starts -->
       <div class="global-fts"></div>
       <nav class="navbar navbar-expand-md">
         <div class="container">
           <!-- Brand and toggle get grouped for better mobile display -->
           <div class="navbar-header">
             <a class="navbar-brand">
               <img src="<?php print $link . '/assets/img/FTS_logo.png' ?>" alt="">
             </a>
             <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
             </button>
           </div>
           <div class="collapse navbar-collapse" id="main-menu">
             <ul class="navbar-nav mr-auto w-100 justify-content-end">
               <li class="nav-item active">
                 <a class="nav-link active" href="#header-wrap">Home <span class="sr-only">(current)</span></a>
               </li>
               <li class="nav-item">
                 <a class="nav-link" href="#features" aria-haspopup="true" aria-expanded="false">Features</a>
               </li>
               <li class="nav-item">
                 <a class="nav-link" href="#blog" aria-haspopup="true" aria-expanded="false">News</a>
               </li>
               <li class="nav-item">
                 <a class="nav-link" href="#documentation"  aria-haspopup="true" aria-expanded="false">Documentation</a>
               </li>
               <li class="nav-item">
                 <a class="nav-link " href="#clients" aria-haspopup="true" aria-expanded="false">Users</a>
               </li>
               <li class="nav-item">
                 <a class="nav-link" href="#contact" aria-haspopup="true" aria-expanded="false">Contact Us</a>
               </li>
             </ul>
           </div>

           <!-- Mobile Menu Start -->
           <ul class="wpb-mobile-menu">
             <li>
               <a class="active" href="#header-wrap">Home</a>
             </li>
             <li>
               <a href="#features">Features</a>
             </li>
             <li>
               <a href="#blog">News</a>
             </li>
             <li>
               <a href="#documentation">Documentation</a>
             </li>
             <li>
               <a href="#clients">Users</a>
             </li>
             <li>
               <a href="#contact">Contact Us</a>
             </li>
           </ul>
           <!-- Mobile Menu End -->
         </div>
       </nav>
       
    <div class="row-index row-atlas" style="height: 800px; background: linear-gradient(0deg,rgba(0, 0, 0, 0.4),rgba(0, 0, 0, 0.4)), url( https://fts.web.cern.ch/sites/fts.web.cern.ch/themes/fts-webpage/assets/img/WLCG.png); background-size: cover !important;">
    <div class="container">
      <div class="row justify-content-md-center">
        <div class="col-md-10" style="padding: 300px 0 80px; color:#fff;">     
          <div class="contents text-center">
              <h1><span>F</span>ile <span>T</span>ransfer <span>S</span>ervice at CERN</h1>
              <h2>
              	Open source software to transfer data reliably and at large scale between storage systems</h2>
          	  <a class="btn btn-border" target="blank_" href="<?php print $link . '/getstarted.html' ?>">
          	  <i class="fa fa-youtube-play"></i>Get Started</a>
          </div>
        </div>
       
      </div>
    </div>
  </div>
  
  
  

    </header>

    <!-- About Us Section -->
    <section id="about" class="section">
      <!-- Container Starts -->
      <div class="container">
        <h1 class="section-title wow fadeIn animated" data-wow-delay=".2s">
        WHY CHOOSE US?
        </h1>
        <p class="section-subcontent"><br>FTS distributes the majority of Large Hadron Collider data<br> across the <a href="http://wlcg.web.cern.ch">Worldwide LHC Computing Grid (WLCG)</a> infrastructure. <br> Developed by CERN.</p>
        <div class="row">
          <div class="col-sm-6 col-md-3">
            <!-- Service-Block-1 Item Starts -->
            <div class="service-item wow fadeInUpQuick animated" data-wow-delay=".5s">
              <div class="icon-wrapper">
                <i class="icon-cup pulse-shrink">
                </i>
              </div>
              <h2>
                Simplicity
              </h2>
              <p> Easy user interaction for submitting transfers with a simple copy format. </p>
              <p> WebFTS portal for end-users and Real Time monitoring. </p>
            </div>
            <!-- Service-Block-1 Item Ends -->
          </div>

          <div class="col-sm-6 col-md-3">
            <!-- Service-Block-1 Item Starts -->
            <div class="service-item wow fadeInUpQuick animated" data-wow-delay=".8s">
              <div class="icon-wrapper">
                <i class="icon-diamond pulse-shrink">
                </i>
              </div>
              <h2>
                Reliability and Integrity
              </h2>
              <p>
                Checksums and retries are provided per transfer.
              </p>
            </div>
            <!-- Service-Block-1 Item Ends -->
          </div>

          <div class="col-sm-6 col-md-3">
            <!-- Service-Block-1 Item Starts -->
            <div class="service-item wow fadeInUpQuick animated" data-wow-delay="1.1s">
              <div class="icon-wrapper">
                <i class="icon-equalizer pulse-shrink">
                </i>
              </div>
              <h2>
                Flexibility and Scalability
              </h2>
              <p> Multiprotocol support (Webdav/https, GridFTP, xroot, SRM). </p>
              <p> Different clients to access the service (curl, python bindings and CPP CLI).</p>
              <p> Transfers from/to different storages (EOS, DPM, Object Storages, STORM, dCache, CTA, ..).</p>
            </div>
            <!-- Service-Block-1 Item Ends -->
          </div>

          <div class="col-sm-6 col-md-3">
            <!-- Service-Block-1 Item Starts -->
            <div class="service-item  wow fadeInUpQuick animated" data-wow-delay="1.4s">
              <div class="icon-wrapper">
                <i class="icon-bulb pulse-shrink">
                </i>
              </div>
              <h2>
                Intelligence
              </h2>
              <p> Parallel transfers optimization to get the most from network without burning the storages.</p>
              <p> Priorities/Activities support for transfers classification. </p>
            </div>
          </div><!-- Service-Block-1 Item Ends -->
        </div>
      </div><!-- Container Ends -->
    </section><!-- Service Main Section Ends -->
    <!-- Cool Facts Section -->
    <section id="cool-facts" class="section" style="margin-right:0px;">
      <!-- Container Starts -->
      <div class="container">
        <h1 class="section-title wow fadeInUpQuick">
          AWESOME NUMBER FACTS
        </h1>
        <!-- Row Starts -->
        <div class="row">
          <div class="col-sm-6 col-md-3 col-lg-3"">
            <!-- Fact Block Starts -->
            <div class="fact-block clearfix wow fadeInUp" data-wow-delay=".3s">
              <div class="facts-item">
                <i class="icon-heart"></i>
                <div class="fact-count">
                  <h3><span class="counter">30</span></h3>
                  <h4>Virtual Organizations</h4>
                </div>
              </div>
            </div><!-- Fact Block Ends -->
          </div>
          <div class="col-sm-6 col-md-3 col-lg-3"">
            <!-- Fact Block Starts -->
            <div class="fact-block clearfix wow fadeInUp" data-wow-delay=".8s">
              <div class="facts-item">
                <i class="icon-layers"></i>
                <div class="fact-count">
                  <h3><span class="counter">24</span>PB</h3>
                  <h4>Volume/week</h4>
                </div>
              </div>
            </div><!-- Fact Block Ends -->
          </div>
          <div class="col-sm-6 col-md-3 col-lg-3"">
            <!-- Fact Block Starts -->
            <div class="fact-block clearfix wow fadeInUp" data-wow-delay="1.1s">
              <div class="facts-item">
                <i class="icon-docs"></i>
                <div class="fact-count">
                  <h3><span class="counter">26</span>M</h3>
                  <h4>Transfers/week</h4>
                </div>
              </div>
            </div><!-- Fact Block Ends -->
          </div>
           <div class="col-sm-6 col-md-3 col-lg-3"">
            <!-- Fact Block Starts -->
            <div class="fact-block clearfix wow fadeInUp" data-wow-delay="1.1s">
              <div class="facts-item">
                <i class="icon-rocket"></i>
                <div class="fact-count">
                  <h3><span class="counter">23</span></h3>
                  <h4>FTS Instances</h4>
                </div>
              </div>
            </div><!-- Fact Block Ends -->
          </div>
        </div><!-- Row Ends -->
      </div><!-- Container Ends -->
    </section>
    <!-- Cool Facts Section End -->
    <!-- Featured Section Starts -->
    <section id="features" class="section">
      <!-- Container Starts -->
      <div class="container">
        <h1 class="section-title wow fadeInUpQuick">
          CORE FEATURES
        </h1>
        <p class="section-subcontent">FTS provides some key components to accomplish the multiple use cases that exist in the scientific community.</p>
        <div class="row">
          <!-- Start Service Icon 1 -->
          <div class="col-md-4 col-sm-6" data-animation="fadeIn" data-animation-delay="01">
            <div class="featured-box">
              <div class="featured-icon">
                <i class="icon-screen-desktop">
                </i>
              </div>
              <div class="featured-content">
                <h4>
                  <a href="https://webfts.cern.ch/">WebFTS</a> - Simplifying power
                </h4>
                <p>
                  WebFTS is a graphical web interface which provides a file transfer and management solution in order to allow users to invoke reliable, managed data transfers on distributed infrastructures.
                </p>
              </div>
            </div>
          </div>
          <!-- End featured Icon 1 -->
          <!-- Start featured Icon 1 -->
          <div class="col-md-4 col-sm-6 wow fadeInUpQuick" data-wow-delay=".2s" data-animation="fadeIn" data-animation-delay="01">
            <div class="featured-box">
              <div class="featured-icon">
                <i class="icon-power">
                </i>
              </div>
              <div class="featured-content">
                <h4>
                  <a href="http://fts3-docs.web.cern.ch/fts3-docs/fts-rest/docs/index.html">FTS-REST</a> - Python bindings
                </h4>
                <p>
                 FTS-REST provides python bindings for easy integration with frameworks and a command-line interface for copying files from one site to another.
                </p>
              </div>
            </div>
          </div>
          <!-- End featured Icon 1 -->
          <!-- Start featured Icon 1 -->
          <div class="col-md-4 col-sm-6 wow fadeInUpQuick" data-wow-delay=".3s" data-animation="fadeIn" data-animation-delay="01">
            <div class="featured-box">
              <div class="featured-icon">
                <i class="icon-graph">
                </i>
              </div>
              <div class="featured-content">
                <h4>
                  <a href="https://monit.cern.ch">Real-Time</a> Monitoring
                </h4>
                <p>
                  FTS provides monitoring for several profiles: <a href="https://monit-grafana.cern.ch/dashboard/db/fts-servers-dashboard?orgId=25">General monitoring</a> (Grafana) for end users, <a href="https://monit-kibana.cern.ch/kibana/app/kibana#/dashboard/MONIT-FTS-Overview">Discovery Data</a> (Kibana) for researches and Service Specific (ftsmon/Kibana) for service managers.
                </p>
              </div>
            </div>
          </div>
          <!-- End featured Icon 1 -->
          <!-- Start featured Icon 1 -->
          <div class="col-md-4 col-sm-6 wow fadeInUpQuick" data-wow-delay=".4s" data-animation="fadeIn" data-animation-delay="01">
            <div class="featured-box">
              <div class="featured-icon">
                <i class="icon-speedometer icon-large icon-effect">
                </i>
              </div>
              <div class="featured-content">
                <h4>
                  <a href="http://fts3-docs.web.cern.ch/fts3-docs/docs/optimizer/optimizer.html">Optimizer</a> - Taking the most from our infrastructure
                </h4>
                <p>
                 The optimizer makes it possible to run transfers between any two random endpoints with good reliability and performance with zero configuration by default.
                </p>
              </div>
            </div>
          </div>
          <!-- End featured Icon 1 -->
          <!-- Start featured Icon 1 -->
          <div class="col-md-4 col-sm-6 wow fadeInUpQuick" data-wow-delay=".5s" data-animation="fadeIn" data-animation-delay="01">
            <div class="featured-box">
              <div class="featured-icon">
                <i class="icon-key">
                </i>
              </div>
              <div class="featured-content">
                <h4><a href="https://dmc.web.cern.ch/projects/gfal-2/home">GFAL</a> - Multiprotocol support</h4>
                <p>
                  GFAL-2 is a plugin based library for file manipulation supporting multiple protocols (Webdav/https, GridFTP, xroot, SRM).
                </p>
              </div>
            </div>
          </div>
          <!-- End featured Icon 1 -->
          <!-- Start featured Icon 1 -->
          <div class="col-md-4 col-sm-6 wow fadeInUpQuick" data-wow-delay=".6s" data-animation="fadeIn" data-animation-delay="01">
            <div class="featured-box">
              <div class="featured-icon">
                <i class="icon-support">
                </i>
              </div>
              <div class="featured-content">
                <h4>
                  <a href="#contact">Support</a>
                </h4>
                <p>
                  FTS support is excellent thanks to the FTS team at CERN.
                </p>
              </div>
            </div>
          </div>
          <!-- End featured Icon 1 -->
        </div>
      </div>
      <!-- Container Ends -->
    </section>
    <!-- Featured Section Ends -->    
    
      <!-- Storage Section -->
    <section id="testimonial" class="section">
      <!-- Container Starts -->
      <div class="container">
        <h1 class="section-title wow fadeInUpQuick">
          STORAGE INTERFACE
        </h1>
        <div class="row">
        	<div class="col-sm-12 wow fadeInUpQuick animated" data-wow-delay=".3s" style="visibility: visible;-webkit-animation-delay: .3s; -moz-animation-delay: .3s; animation-delay: .3s;">
            <div id="testimonial-item" class="owl-carousel" style="opacity: 1; display: block;">
              <div class="item">
                <div class="testimonial-inner">
                  <div class="testimonial-images">
                    <img src="<?php print '/' . path_to_theme() . '/assets/img/storages/CLOUDS.png' ?>"alt="">
                  </div>
                  <div class="testimonial-content">
                    <p>
                    Any cloud storage in the market with S3 compatiblity.
                    </p>
                  </div>
                </div>
              </div>
               <div class="item">
                <div class="testimonial-inner">
                  <div class="testimonial-images">
                    <a href="http://lcgdm.web.cern.ch/dpm"><img src="<?php print '/' . path_to_theme() . '/assets/img/storages/DPM.png' ?>"alt=""></a>
                  </div>
                  <div class="testimonial-content">
                    <p>
         				Disk Pool Manager: Storage system for grid sites.
                    </p>
                  </div>
                  <div class="testimonial-footer">
                    developed by CERN
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimonial-inner">
                  <div class="testimonial-images">
                    <a href="https://eos.web.cern.ch"><img src="<?php print '/' . path_to_theme() . '/assets/img/storages/EOS1.png' ?>"alt=""></a>
                  </div>
                  <div class="testimonial-content">
                    <p>
                      EOS Open Storage: Large disk storage at CERN.
                    </p>
                  </div>
                  <div class="testimonial-footer">
                    developed by CERN</a>
                  </div>
                </div>
              </div> 
              <div class="item">
                <div class="testimonial-inner">
                  <div class="testimonial-images">
                    <a href="http://italiangrid.github.io/storm/" target="_blank"><img src="<?php print '/' . path_to_theme() . '/assets/img/storages/STORM.png' ?>"alt=""></a>
                  </div>
                  <div class="testimonial-content">
                    <p>
                      Storage Resource Management. 
                    </p>
                  </div>
                  <div class="testimonial-footer">
                    
                    developed by INFN
                  </div>
                </div>
              </div>
            <div class="item">
                <div class="testimonial-inner">
                  <div class="testimonial-images">
                    <a href="http://castor.web.cern.ch" target="_blank"><img src="<?php print '/' . path_to_theme() . '/assets/img/storages/CASTOR.png' ?>"alt=""></a>
                  </div>
                  <div class="testimonial-content">
                    <p>
                     The CERN Advanced STORage manager based on tapes.
                    </p>
                  </div>
                  <div class="testimonial-footer">
                    
                    developed by CERN
                  </div>
                </div>
                </div>
                <div class="item">
                <div class="testimonial-inner">
                  <div class="testimonial-images">
                    <a href="https://www.dcache.org/" target="_blank"><img src="<?php print '/' . path_to_theme() . '/assets/img/storages/DCACHE.png' ?>"alt=""></a>
                  </div>
                  <div class="testimonial-content">
                    <p>
                      A single virtual filesystem tree.
                    </p>
                  </div>
                  <div class="testimonial-footer">
                    
                    developed by DESY
                  </div>
                </div>
              </div>
              </div>
              </div>      
          </div>
        </div>
      </div>
    </section> 
    
      <!-- Documentation Section -->
    <section id="documentation" class="section">
      <div class="container">
        <h1 class="section-title wow fadeInUpQuick">
          WANT TO KNOW MORE?
        </h1>
        <div class="row">
           <div class="col-md-4">
            <div class="download-area text-center wow fadeInUp animated" data-wow-delay="0.3s" style="visibility: visible;-webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
                <a href="http://fts3-docs.web.cern.ch/fts3-docs/" target="_blank" class="btn btn-border"><i class="fa fa-book"></i>Documentation</a>
            </div>
          </div>
          <div class="col-md-4">
            <div class="download-area text-center wow fadeInUp animated" data-wow-delay="0.3s" style="visibility: visible;-webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
                <a href="https://indico.cern.ch/category/6650/" target="_blank" class="btn btn-border"><i class="fa fa-file-powerpoint-o"></i>Steering Meetings</a>
            </div>
          </div>
          <div class="col-md-4">
            <div class="download-area text-center wow fadeInUp animated" data-wow-delay="0.3s" style="visibility: visible;-webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
                <a href="<?php print '/' . path_to_theme() . '/releases-jekyll/releases/index.html' ?>" target="_blank" class="btn btn-border"><i class="fa fa-book"></i>Releases</a>
            </div>
          </div>
          <div class="col-md-6">
            <div class="download-area text-center wow fadeInUp animated" data-wow-delay="0.3s" style="visibility: visible;-webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
                <a href="https://cernbox.cern.ch/index.php/s/vOIhn0zJZHToIbm" target="_blank" class="btn btn-border"><i class="fa fa-book"></i>Publications</a>
            </div>
          </div>
          <div class="col-md-6">
            <div class="download-area text-center wow fadeInUp animated" data-wow-delay="0.3s" style="visibility: visible;-webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
                <a href="<?php print '/' . path_to_theme() . '/techblog-jekyll/techblog/index.html' ?>" target="_blank" class="btn btn-border"><i class="fa fa-book"></i>TechBlog</a>
            </div>
          </div>
          
        </div>
      </div>
    </section>
    
    <!-- News Section -->
    <section id="blog" class="section">
      <!-- Container Starts -->
      <div class="container">
        <h1 class="section-title wow fadeInUpQuick">
          LATEST NEWS
        </h1>
        <p class="section-subcontent">Here we show the latest news. All news can be accesible <a href="<?php print $link . '/news.html' ?>">here</a></p>   
        <!-- Row Starts -->
        <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <!-- Blog Item Starts -->
            <div class="blog-item-wrapper wow fadeIn" data-wow-delay="0.3s">
              <div class="blog-item-img">
                <a href="#">
                  <img src="<?php print $link . '/assets/img/news/xdc_logo.png' ?>" width="150" height="150" alt="">
                </a>
              </div>
              <div class="blog-item-text">
                <h3 class="small-title"><a href="#">FTS has joined the XDC Project</a></h3>
                <p>
                  FTS has joined forces with the XDC Project to bring data movement and QoS management to the next generation of scientific data infrastructures.<br><br>
                </p>
                <div class="blog-one-footer">
                 	<a href="http://www.extreme-datacloud.eu/">Read More</a>                 
                </div>
                
              </div>
            </div><!-- Blog Item Wrapper Ends-->
          </div>

          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <!-- Blog Item Wrapper Start-->
            <div class="blog-item-wrapper wow fadeIn" data-wow-delay="0.6s">
              <div class="blog-item-img">
                <a href="#">
                   <img src="<?php print $link . '/assets/img/news/NA62.jpg' ?>" width="150" height="150" alt="">
                </a>
              </div>
              <div class="blog-item-text">
                <h3 class="small-title"><a href="http://cern.ch/go/7SwG">NA62 is transfering 4-10 TiB per day </a></h3>
                <p>
                   NA62 now uses FTS for Data AcQuisition, thanks to our new DAQ FTS instance for that purpose.<br><br><br>
                </p>
                <div class="blog-one-footer">
                 	<a href="http://cern.ch/go/7SwG">Read More</a>                 
                </div>
              </div>
            </div><!-- Blog Item Wrapper Ends-->
          </div>

          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <!-- Blog Item Wrapper Starts-->
            <div class="blog-item-wrapper wow fadeIn" data-wow-delay="0.9s">
              <div class="blog-item-img">
                <a href="#">
                  <img src="<?php print $link . '/assets/img/news/Tapes.jpg' ?>" width="150" height="150" alt="">
                </a>
              </div>
              <div class="blog-item-text">
               	<h3 class="small-title"><a href="https://indico.cern.ch/event/578974/contributions/2695183/attachments/1519628/2377639/FTS_as_interface_for_achival_storage.pdf">FTS as the interface to archival storage</a></h3>
                <p>
                   FTS is providing common metrics for different tapes systems to help experiments answer their question what they can expect from tapes.
                </p>
                <div class="blog-one-footer">
                 	<a href="https://indico.cern.ch/event/578974/contributions/2695183/attachments/1519628/2377639/FTS_as_interface_for_achival_storage.pdf">Read More</a>                 
                </div>
              </div>
            </div><!-- Blog Item Wrapper Ends-->
          </div>
        </div><!-- Row Ends -->
      </div><!-- Container Ends -->
    </section>
    <!-- blog Section End -->

    <!-- Clients Section -->
    <section id="clients" class="section">
      <!-- Container Ends -->
      <div class="container">
        <h1 class="section-title wow fadeInUpQuick" data-wow-delay=".5s">
          OUR USERS
        </h1>
        <p class="section-subcontent">Our users/researchers belong to different Virtual Organizations (VOs) and experiments. <br></p>
         <div class="wow fadeInUpQuick" data-wow-delay=".9s"> 
          <!-- Row and Scroller Wrapper Starts -->
          <div class="row">
              <div id="clients-scroller" class="row">
				 <div class="client-item-wrapper">
					<a href="http://atlas.cern/"><img src="<?php print $link . '/assets/img/clients/ATLAS.png' ?>" alt=""></a>
				  </div>	
				  <div class="client-item-wrapper">
					<a href="http://cms.cern/"><img src="<?php print $link . '/assets/img/clients/CMS.png' ?>" alt=""></a>
				  </div>
				  <div class="client-item-wrapper">
					<a href="https://lhcb.web.cern.ch/lhcb/"><img src="<?php print $link . '/assets/img/clients/LHCB.png' ?>" alt=""></a>
				  </div>
				  <div class="client-item-wrapper">
					<a href="https://na62.web.cern.ch/na62/"><img src="<?php print $link . '/assets/img/clients/NA62.png' ?>" alt=""></a>
				  </div>
				  <div class="client-item-wrapper">
					<a href="http://www.ams02.org"><img src="<?php print $link . '/assets/img/clients/AMS.png' ?>" alt=""></a>
				  </div>
				  <div class="client-item-wrapper">
					<a href="https://www.belle2.org/"><img src="<?php print $link . '/assets/img/clients/BELLE.png' ?>" alt=""></a>
				  </div>
				  <div class="client-item-wrapper">
					<a href="https://www.linearcollider.org"><img src="<?php print $link . '/assets/img/clients/ILC.png' ?>" alt=""></a>
				  </div>
				   <div class="client-item-wrapper">
					<a href="http://wwwcompass.cern.ch"><img src="<?php print $link . '/assets/img/clients/COMPASS.png' ?>" alt=""></a>
				  </div>
				  <div class="client-item-wrapper">
					<a href="https://www.gridpp.ac.uk/"><img src="<?php print $link . '/assets/img/clients/GridPP.jpeg' ?>" alt=""></a>
				  </div>
				  <div class="client-item-wrapper">
					<a href="https://projectescape.eu/"><img src="<?php print $link . '/assets/img/clients/Escape.png' ?>" alt=""></a>
				  </div>
				  <div class="client-item-wrapper">
					<a href="https://www.skatelescope.org/"><img src="<?php print $link . '/assets/img/clients/SKA.jpeg' ?>" alt=""></a>
				  </div>
				  <div class="client-item-wrapper">
					<a href="https://www.dunescience.org/"><img src="<?php print $link . '/assets/img/clients/DUNE.jpg' ?>" alt=""></a>
				  </div>
				  <div class="client-item-wrapper">
					<a href="http://juno.ihep.cas.cn/"><img src="<?php print $link . '/assets/img/clients/JUNO.jpeg' ?>" alt=""></a>
				  </div>	
				  <div class="client-item-wrapper">
					<a href="https://www.ligo.caltech.edu/"><img src="<?php print $link . '/assets/img/clients/Ligo.png' ?>" alt=""></a>
				  </div>	
				  <div class="client-item-wrapper">
					<a href="http://www.virgo-gw.eu/"><img src="<?php print $link . '/assets/img/clients/Virgo.png' ?>" alt=""></a>
				  </div>	
				  <div class="client-item-wrapper">
					<a href="https://www.auger.org/"><img src="<?php print $link . '/assets/img/clients/AUGER.png' ?>" alt=""></a>
				  </div>				  
       		 </div>

          <!-- Row and Scroller Wrapper Starts -->
        </div>
    </div>
  </div><!-- Container Ends -->
</section>    <!-- Client Section End -->



    <!-- Footer Section -->
    <footer>
    <section id="contact" class="section">
      <div class="container">
      
        <div class="row justify-content-md-center">
          <div class="col-md-9" style="visibility: visible;-webkit-animation-duration: 1000ms; -moz-animation-duration: 1000ms; animation-duration: 1000ms;-webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
            <div class="contact-block">
             <h1 class="section-title wow fadeInUpQuick">CONTACT US</h1>
              <!-- MAP, EMAIL and PHONE section -->

              <div class="col-md-12">
                  <div class="contact_detail wow animated fadeInUp animated" style="visibility: visible;">
                  <div class="row">
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="contact_detail_m">
                              <i class="icon-map"></i><br>
                              <a href="https://goo.gl/maps/DUt37zN3UNK2" target="_blank"><p>Route de Meyrin, 1217 Meyrin, Geneva</p></a>
                          </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="contact_detail_m">
                              <i class="icon-envelope"></i>
                              <a href="mailto:fts-support@cern.ch"><p>fts-support</p></a>
                          </div>
                      </div>
                      </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

      <!-- Copyright -->
      <div id="copyright">
        <div class="container">
          <div class="row">
            <div class="col-md-6 col-sm-6">
              <p class="copyright-text">
                ©  2017 CERN. All right reserved.
              </p>
            </div>
            <div class="col-md-6  col-sm-6">
              <ul class="nav nav-inline  justify-content-end ">
                <li class="nav-item">
                  <a class="nav-link active" href="#">Home</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <!-- Copyright  End-->

    </footer>
    <!-- Footer Section End-->

    <!-- Go To Top Link -->
    <a href="#" class="back-to-top">
      <i class="fa fa-angle-up">
      </i>
    </a>

    <!-- JavaScript & jQuery Plugins -->
    <script src="<?php print $link . '/assets/js/jquery-min.js' ?>"></script>
    <!-- Tether Js -->
    <script src="<?php print $link . '/assets/js/tether.min.js' ?>"></script>
    <!-- Bootstrap JS -->
    <script src="<?php print $link . '/assets/js/bootstrap.min.js' ?>"></script>
    <!--Text Rotator-->
    <script src="<?php print $link . '/assets/js/jquery.mixitup.js' ?>"></script>
    <!--WOW Scroll Spy-->
    <script src="<?php print $link . '/assets/js/wow.js' ?>"></script>
    <!-- OWL Carousel -->
    <script src="<?php print $link . '/assets/js/owl.carousel.js' ?>"></script>
    <!-- WayPoint -->
    <script src="<?php print $link . '/assets/js/waypoints.min.js' ?>"></script>
    <!-- CounterUp -->
    <script src="<?php print $link . '/assets/js/jquery.counterup.min.js' ?>"></script>
    <!-- Slicknav -->
    <script src="<?php print $link . '/assets/js/jquery.counterup.min.js' ?>"></script>
    <!-- ScrollTop -->
    <script src="<?php print $link . '/assets/js/jquery.slicknav.js' ?>"></script>
    <!-- Appear -->
    <script src="<?php print $link . '/assets/js/jquery.appear.js' ?>"></script>
    <!-- Vide js -->
    <script src="<?php print $link . '/assets/js/jquery.vide.js' ?>"></script>
     <!-- All JS plugin Triggers -->
    <script src="<?php print $link . '/assets/js/main.js' ?>"></script>

  </body>
</html>

