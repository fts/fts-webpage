FTS Webpage
===========================
- The first version for FTS  webpage was created in OCT 2017
- Description:File Transfer Service at CERN
- Author: Maria Arsuaga Rios

Adding a new release or techblog post:
Go to _posts directory and create a new file with the same format as the rest.
In case of releases you just need to copy and paste the JIRA release notes to the files and update the header.
Then later pull in fts.web.cern.ch and run bash deploy.sh to have it in the production webpage.
And enjoy!