#!/bin/bash
#requires ruby 2.1

ruby -v
which ruby
bundle install

cd techblog-jekyll
jekyll build --destination techblog
cd ..
cd releases-jekyll
jekyll build --destination releases
cd ..
