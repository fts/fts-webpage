---
layout:     post
title:      "FTS 3.7.9"
subtitle:   ""
date:       2018-05-05 12:00:00
author:     "FTS Team"
tags:       [CORE, PRODUCTION]

---
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1196'>FTS-1196</a>] -         fts-transfer-submit -e option issue
</li>
</ul>
                                                       
