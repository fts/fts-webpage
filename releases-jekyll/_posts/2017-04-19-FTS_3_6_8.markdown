---
layout:     post
title:      "FTS 3.6.8"
subtitle:   ""
date:       2017-04-19 12:00:00
author:     "FTS Team"
tags:       [CORE, PRODUCTION, EPEL]

---
<h2>Bug</h2>

<ul>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-919">FTS-919</a>] - Permissions on Proxy file are too permissive for the LFC plugin to work</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-920">FTS-920</a>] - fts-client help misbehavior</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-933">FTS-933</a>] - fts-delegation-init --proxy does not work with just the file name</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-935">FTS-935</a>] - Improve some queries in the web monitoring</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-945">FTS-945</a>] - 3.6 breaks Dropbox integration</li>
</ul>

<h2>New Feature</h2>

<ul>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-926">FTS-926</a>] - Publish error codes</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-929">FTS-929</a>] - DB Clean binary should log progress</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-930">FTS-930</a>] - Upgrade script should double check t_hosts before running .sql diff</li>
</ul>

<h2>Task</h2>

<ul>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-937">FTS-937</a>] - improve performance of fts3.6.0 schema diff and provide partial upgrade script</li>
</ul>

<h2>Improvement</h2>

<ul>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-932">FTS-932</a>] - Drop "database statistics" view</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-936">FTS-936</a>] - Reorder group by in getQueuesWithPending to benefit from group by with index</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-939">FTS-939</a>] - Log when a message is sent</li>
</ul>
