---
layout:     post
title:      "FTS-REST 3.7.1"
subtitle:   ""
date:       2017-10-09 12:00:00
author:     "FTS Team"
tags:       [REST, PRODUCTION]

---
<p>Release Notes - FTS - Version&nbsp;</p>

<h2>Bug</h2>

<ul>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-1014">FTS-1014</a>] - Link configuration page does not work</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-1021">FTS-1021</a>] - Cannot remove SE configuration from Web Config</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-1022">FTS-1022</a>] - SE limits for * configuration are not shown on the Web Conf</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-1055">FTS-1055</a>] - Cannot add link config via Web Config</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-1056">FTS-1056</a>] - Cannot delete some link configs</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-1061">FTS-1061</a>] - Web Conf: duplicate field</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-1069">FTS-1069</a>] - Cannot delete shares from web config</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-1072">FTS-1072</a>] - FTS writes messages to /var/lib/fts3/monitoring even though MonitoringMessaging is set to false</li>
</ul>

<h2>Task</h2>

<ul>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-1041">FTS-1041</a>] - Add an option to fts-rest-delegate to specify the proxy duration</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-1047">FTS-1047</a>] - Indicate json files with alternative curl configurations instead of using strings</li>
</ul>
