---
layout:     post
title:      "FTS-REST 3.4.1"
subtitle:   ""
date:       2016-01-22 12:00:00
author:     "FTS Team"
tags:       [REST, PRODUCTION, EPEL]

---
<p>Release Notes - FTS - Version fts-rest 3.4.1</p>

<h2>Bug</h2>

<ul>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-430">FTS-430</a>] - Explicitly request application/json on the configuration web UI</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-441">FTS-441</a>] - Use "READ COMMITTED" isolation level</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-443">FTS-443</a>] - Make sure transactions are closed when the session falls out of scope</li>
</ul>

<h2>New Feature</h2>

<ul>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-372">FTS-372</a>] - Namespaced uuid version 5 for Atlas</li>
</ul>
