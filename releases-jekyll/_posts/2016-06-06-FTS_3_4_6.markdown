---
layout:     post
title:      "FTS 3.4.6"
subtitle:   ""
date:       2016-06-06 12:00:00
author:     "FTS Team"
tags:       [CORE, PRODUCTION]

---
<h2>Bug</h2>

<ul>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-584">FTS-584</a>] - Excessive logging when a status message is consumed after fts_url_copy finished</li>
</ul>




