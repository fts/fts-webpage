---
layout:     post
title:      "FTS 3.2.33"
subtitle:   ""
date:       2015-03-03 12:00:00
author:     "FTS Team"
tags:       [CORE, REST, PRODUCTION]

---
<p>This is mostly a bug-fix release, with particularly important improvements on the REST API.</p>

<p><a href="https://svnweb.cern.ch/trac/fts3/milestone/FTS3%203.2.33">FTS3 Release Notes</a></p>

<p><a href="https://github.com/cern-it-sdc-id/fts3-rest/blob/stage/docs/releases/v3.2.33.md">FTS3-Rest Release Notes</a></p>

<p>&nbsp;</p>