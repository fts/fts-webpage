---
layout:     post
title:      "FTS3 REST 3.2.3"
subtitle:   ""
date:       2014-05-16 12:00:00
author:     "FTS Team"
tags:       [REST, PRODUCTION]

---
<p><img alt="" longdesc="" src="{{ site.baseurl | prepend: site.url }}/img/sonarqube.png" style="float: right; height: 368px; width: 550px;" />It took a while, but finally I have got the coverage information showing up in our <a href="http://www.sonarqube.org/">Sonarqube</a> <a href="http://baluarte.cern.ch:8080/">instance</a>&nbsp;(only accessible within CERN network).</p>

<p>Now we have the coverage information for FTS3, FTS3 REST API and GFAL2.</p>

<p>Extracting the coverage information for the Python code was fairly easy: just run nosetests with the option<em> </em><em>--with-coverage --cover-xml&nbsp;--cover-xml-file=coverage.xml&nbsp;</em>and there you go. Of course, you need to configure the sonar-project.properties properly.</p>

<pre>
<span style="line-height: 1.6;">sonar.core.codeCoveragePlugin=cobertura</span>

sonar.python.xunit.reportPath=nosetests.xml
sonar.python.coverage.reportPath=coverage.xml</pre>

<p>However, extracting the coverage information for the C/C++ code (both for FTS3 and GFAL2) was a little bit trickier, specially for FTS3, since we need to run the server side compiled with the coverage instrumentation.</p>

<p>Basically, these were the steps:</p>

<h3>Compile with coverage instrumentation</h3>

<p>Pice of cake. Just compile as follows:</p>

<pre>
CFLAGS=--coverage CXXFLAGS=--coverage cmake "${SOURCE_DIR}" \
    -DMAINBUILD=ON -DSERVERBUILD=ON -DCLIENTBUILD=ON -DMYSQLBUILD=ON \
    -DTESTBUILD=ON
make -j2</pre>

<p>With that, pretty much done for GFAL2. After the build, run ctest and you have the coverage data (more on how to send this coverage data to Sonarqube later). However, FTS3 must be started as a server before we get integration coverage.</p>

<h3>Build inside a <a href="https://fedoraproject.org/wiki/Mock">mock environment</a></h3>

<p>Why? Because we need to install a bunch of dependencies without polluting the build machine.</p>

<h3>Run the server inside the mock environment</h3>

<p>The FTS3 code assumed several things that made running the compiled code just for the tests harder. For instance:</p>

<ol>
	<li>Always switches to user and group fts3. It requires, of course, the user and group fts3 to exists on the node, which is not true for the CI machines. Also, if you build as root and then switch to fts3, the coverage data can not be collected.</li>
	<li>SOAP interface always run, which requires permissions to bind, a valid certificate...
	<ol>
		<li>Eventually this could be done, but we are deprecating anyway</li>
	</ol>
	</li>
	<li>Assumes log directory location, permissions,...</li>
</ol>

<p>&nbsp;</p>

<p>So I had to modify several bits on the code to make it easier to run FTS3 in this limited environment.</p>

<h3><span style="line-height: 1.6;">Run the tests</span></h3>

<p><span style="line-height: 20.8px;">There is an external REST host listening for the requests that the test instance will run, since they share the DB. Jobs are submitted there, and picked by the instrumented FTS3.</span></p>

<h3><span style="line-height: 20.8px;">Shut down the services</span></h3>

<p>Sound easy, right? It wasn't. In order to store the coverage data, the process needs to terminate normally (this is, exit being called). So far so good. But, oh my, FTS3 tended to use _exit instead, which will *not* call cleanup and destructors, so no coverage data.</p>

<p>Bummer. Well, <em>sed s/_exit/exit/g</em> and it should be good, shouldn't it? Nope. It turned out that if you called exit, then destructors are called, which is what we wanted. But that triggered a lot of race conditions on the tear down of the services. Meaning, segfaults. Meaning, no coverage either.</p>

<p>So I had to go through a lot of debugging until I managed to get rid of these race conditions, and get a nicely ordered shut down of the service. With this, the coverage data is generated.</p>

<h3>Colllect the coverage</h3>

<p>Once here, we have a bunch of .gcda files which we need to aggregate. We use <a href="http://ltp.sourceforge.net/coverage/lcov.php">lcov</a> first to aggregate into an .info, and&nbsp;<a href="https://github.com/eriwen/lcov-to-cobertura-xml">lcov_covertura.py</a> to generate an XML that can be parsed by, for instance, Jenkins... but not by the <a href="https://github.com/SonarOpenCommunity/sonar-cxx">Sonarqube Community CXX Plugin</a>.</p>

<p>Luckily, that was easy. Just use an existing XSLT that transforms one into the other, and good to go. This is how it looks on the sonar-project.properties file</p>

<pre>
sonar.core.codeCoveragePlugin=cobertura

sonar.cxx.cppcheck.reportPath=cppcheck.xml
sonar.cxx.rats.reportPath=rats.xml
sonar.cxx.vera.reportPath=vera.xml
sonar.cxx.pclint.reportPath=pclint.xml
sonar.cxx.compiler.parser=gcc
sonar.cxx.compiler.reportPath=build.log
sonar.cxx.coverage.reportPath=coverage-unit.xml
sonar.cxx.coverage.itReportPath=coverage-integration.xml
sonar.cxx.coverage.overallReportPath=coverage-overall.xml
sonar.cxx.xunit.reportPath=tests.xml
sonar.cxx.xunit.xsltURL=https://raw.githubusercontent.com/SonarOpenCommunity/sonar-cxx/master/sonar-cxx-plugin/src/main/resources/xsl/boosttest-1.x-to-junit-1.0.xsl</pre>

<h3>Putting all together</h3>

<p>Four scripts put everything together so it can be easily run: <a href="https://gitlab.cern.ch/fts/fts3/blob/develop/coverage.sh">coverage.sh</a>, <a href="https://gitlab.cern.ch/fts/fts3/blob/develop/coverage-unit.sh">coverage-unit.sh</a>, <a href="https://gitlab.cern.ch/fts/fts3/blob/develop/coverage-integration.sh">coverage-integration.sh</a> and <a href="https://gitlab.cern.ch/fts/fts3/blob/develop/coverage-overall.sh">coverage-overall.sh</a>. An <a href="https://gitlab.cern.ch/fts/build-utils/blob/master/jenkins/qa-fts3.sh">additional one</a> orchestrates the whole process in Jenkins.</p>

<p>&nbsp;</p>
