---
layout:     post
title:      "FTS 3.2.26"
subtitle:   ""
date:       2014-09-15 12:00:00
author:     "FTS Team"
tags:       [CORE, PRODUCTION]

---
<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">*Many REST interface optimizations<br />
*Bulk files staging<br />
*Per VO retry global mechanism<br />
*Many web monitoring performance improvements and new features<br />
*Use Exponential Moving Average (EMA) along with weighted-average to avoid frequent "slopes" in the auto-tuning algorithm<br />
*Crash fixed with concurrent delegations<br />
*Log ip of the users in the log file<br />
*Support multi-op and multiple replica jobs using glite/fts3 clients<br />
*Remove weak ciphers from openssl<br />
*fts-transfer-list to include source_se, dest_se in the params list<br />
*Specify max active for a given link, will vary between 2-MAX&nbsp;&nbsp; &nbsp;<br />
*Many MySql optimizations</p>

<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">&nbsp;</p>

<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-weight: 700; background: transparent;">-Apply the database changes below</span></p>

<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">SET storage_engine=INNODB;</p>

<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">ALTER TABLE t_optimize_active ADD ema &nbsp; DOUBLE DEFAULT 0;</p>

<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">ALTER TABLE t_optimizer_evolution ADD agrthroughput &nbsp; FLOAT DEFAULT NULL;</p>

<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">CREATE INDEX t_optimize_active_datetime&nbsp; ON t_optimize_active(datetime);&nbsp;</p>

<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">DROP TABLE t_stage_req;</p>

<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">CREATE TABLE `t_stage_req` (<br />
&nbsp; `vo_name` varchar(100) NOT NULL,<br />
&nbsp; `host` varchar(150) NOT NULL,<br />
&nbsp; `operation` varchar(150) NOT NULL,<br />
&nbsp; `concurrent_ops` int(11) DEFAULT '0',<br />
&nbsp; PRIMARY KEY (`vo_name`,`host`,`operation`)<br />
);</p>

<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">CREATE TABLE `t_turl` (</p>

<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">&nbsp; `source_surl` varchar(150) NOT NULL,<br />
&nbsp; `destin_surl` varchar(150) NOT NULL,<br />
&nbsp; `source_turl` varchar(150) NOT NULL,<br />
&nbsp; `destin_turl` varchar(150) NOT NULL,<br />
&nbsp; `datetime` timestamp NULL DEFAULT NULL,<br />
&nbsp; `throughput` float DEFAULT NULL,<br />
&nbsp; `finish` double DEFAULT '0',<br />
&nbsp; `fail` double DEFAULT '0',<br />
&nbsp; PRIMARY KEY (`source_surl`,`destin_surl`,`source_turl`,`destin_turl`),<br />
&nbsp; KEY `t_url_datetime` (`datetime`),<br />
&nbsp; KEY `t_url_finish` (`finish`),<br />
&nbsp; KEY `t_url_fail` (`fail`)<br />
);</p>

<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">ALTER TABLE t_server_config ADD vo_name VARCHAR(100);</p>

<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">DROP TABLE t_dm;</p>

<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">CREATE TABLE `t_dm` (<br />
&nbsp; `file_id` int(11) NOT NULL AUTO_INCREMENT,<br />
&nbsp; `job_id` char(36) NOT NULL,<br />
&nbsp; `file_state` varchar(32) NOT NULL,<br />
&nbsp; `dmHost` varchar(150) DEFAULT NULL,<br />
&nbsp; `source_surl` varchar(900) DEFAULT NULL,<br />
&nbsp; `dest_surl` varchar(900) DEFAULT NULL,<br />
&nbsp; `source_se` varchar(150) DEFAULT NULL,<br />
&nbsp; `dest_se` varchar(150) DEFAULT NULL,<br />
&nbsp; `reason` varchar(2048) DEFAULT NULL,<br />
&nbsp; `checksum` varchar(100) DEFAULT NULL,<br />
&nbsp; `finish_time` timestamp NULL DEFAULT NULL,<br />
&nbsp; `start_time` timestamp NULL DEFAULT NULL,<br />
&nbsp; `job_finished` timestamp NULL DEFAULT NULL,<br />
&nbsp; `tx_duration` double DEFAULT NULL,<br />
&nbsp; `retry` int(11) DEFAULT '0',<br />
&nbsp; `user_filesize` double DEFAULT NULL,<br />
&nbsp; `file_metadata` varchar(1024) DEFAULT NULL,<br />
&nbsp; `activity` varchar(255) DEFAULT 'default',<br />
&nbsp; `dm_token` varchar(255) DEFAULT NULL,<br />
&nbsp; `retry_timestamp` timestamp NULL DEFAULT NULL,<br />
&nbsp; `wait_timestamp` timestamp NULL DEFAULT NULL,<br />
&nbsp; `wait_timeout` int(11) DEFAULT NULL,<br />
&nbsp; `hashed_id` int(10) unsigned DEFAULT '0',<br />
&nbsp; `vo_name` varchar(100) DEFAULT NULL,<br />
&nbsp; PRIMARY KEY (`file_id`),<br />
&nbsp; KEY `dm_job_id` (`job_id`),<br />
&nbsp; KEY `t_dm_all` (`vo_name`,`source_se`,`file_state`),<br />
&nbsp; KEY `t_dm_source_Se` (`source_se`,`file_state`),<br />
&nbsp; KEY `t_dm_state` (`file_state`,`hashed_id`),<br />
&nbsp; CONSTRAINT `t_dm_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `t_job` (`job_id`)<br />
);</p>

<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">CREATE TABLE t_dm_backup AS (SELECT * FROM t_dm);</p>

<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-weight: 700; background: transparent;">-Update all packages below</span></p>

<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">fts-*</p>

<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">gfal2-*</p>

<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">srm-ifce</p>

<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">CGSI-gSOAP</p>

<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">davix-*</p>

<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-weight: 700; background: transparent;">-Restart services</span></p>

<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">fts-bringonline</p>

<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">fts-msg-bulk</p>

<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">fts-records-cleaner</p>

<p style="border: 0px; font-size: 15px; margin: 0px 0px 0.8em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(34, 34, 34); font-family: Verdana, Arial, sans-serif; line-height: 18px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">fts-server</p>
