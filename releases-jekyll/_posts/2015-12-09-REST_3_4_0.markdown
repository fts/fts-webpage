---
layout:     post
title:      "FTS-REST 3.4.0"
subtitle:   ""
date:       2015-12-09 12:00:00
author:     "FTS Team"
tags:       [REST, PRODUCTION, EPEL]

---
<h2>Bug</h2>

<ul>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-269">FTS-269</a>] - /jobs/&lt;job-id&gt;/files leaks db connections</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-270">FTS-270</a>] - IntegrityError on submission when another job inserted the same storage pair in t_optimizer_active</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-282">FTS-282</a>] - REST CLI: -K is a flag, but it is expecting a value</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-291">FTS-291</a>] - Forbid reuse jobs with different hosts</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-296">FTS-296</a>] - Use SHA256 for signing certificate request</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-297">FTS-297</a>] - When un-banning a storage, need to set wait_timestamp to Null</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-307">FTS-307</a>] - Delegation on Oracle fails with a 500 when there is an existing credential in the cache</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-341">FTS-341</a>] - voms_attrs empty for voms proxies delegated to FTS</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-342">FTS-342</a>] - When using RFC proxies in http authentication the user_dn is incorrect</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-358">FTS-358</a>] - FTS REST APIs do not support alternative ca certificates locations</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-394">FTS-394</a>] - Connection validation must be called on checkout</li>
</ul>

<h2>Configuration Change</h2>

<ul>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-289">FTS-289</a>] - Allow bring online jobs for mock://</li>
</ul>

<h2>Improvement</h2>

<ul>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-247">FTS-247</a>] - Use sqlalchemy 0.8 available in epel6</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-254">FTS-254</a>] - Allow to enable IPv6 at submission time</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-274">FTS-274</a>] - Add --capath to the fts rest CLI</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-293">FTS-293</a>] - Expose deletion jobs in the easy API</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-299">FTS-299</a>] - REST Client: If X509_USER_PROXY is not present, try with the default location</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-300">FTS-300</a>] - Allow to modify a job priority while it is on the queue</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-301">FTS-301</a>] - Add a tool that evaluates if a host is still running something</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-312">FTS-312</a>] - When accessing with the host cert, grant full permissions</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-405">FTS-405</a>] - Migrate from pycurl to 'requests'</li>
</ul>

<h2>Incident</h2>

<ul>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-365">FTS-365</a>] - Status of ATLAS Canceled jobs @RAL and @CERN cannot be queried via REST</li>
</ul>

<h2>New Feature</h2>

<ul>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-221">FTS-221</a>] - Allow to retrieve files/jobs with a given destination surl</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-234">FTS-234</a>] - Small tool that queries monitoring to see active limits</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-272">FTS-272</a>] - Activity shares missing in the REST config API</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-290">FTS-290</a>] - Expose retry_delay on submission</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-316">FTS-316</a>] - New algorithms to select best replica at submission time</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-321">FTS-321</a>] - REST CLI: Use hostcert.pem/hostkey.pem and localhost if no parameters are specified</li>
</ul>

<h2>Task</h2>

<ul>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-266">FTS-266</a>] - Add FTS dropbox/s3 configuration to FTS Web Management interface</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-268">FTS-268</a>] - Enable by default ExportCertData</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-317">FTS-317</a>] - Add order by descending finish_time when showing jobs and a limit is specified</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-320">FTS-320</a>] - Extend HTTP authorization to accept proxy chain</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-356">FTS-356</a>] - need REST equivalent of gSOAP ifce cancelAll</li>
</ul>
