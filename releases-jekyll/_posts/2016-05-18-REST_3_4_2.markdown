---
layout:     post
title:      "FTS-REST 3.4.2"
subtitle:   ""
date:       2016-05-18 12:00:00
author:     "FTS Team"
tags:       [REST, PRODUCTION]

---
<h2>Bug</h2>

<ul>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-539">FTS-539</a>] - Provide a 409 error code for sid duplication instead of 500.</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-548">FTS-548</a>] - Failure to submit when a job combines banned and non banned storages</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-561">FTS-561</a>] - Missing 'STARTED' in FileActiveStates</li>
</ul>

<h2>Improvement</h2>

<ul>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-435">FTS-435</a>] - Return 503 instead of blocking if the server/db is highly load</li>
</ul>



