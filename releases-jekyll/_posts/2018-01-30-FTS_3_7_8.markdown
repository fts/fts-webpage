---
layout:     post
title:      "FTS 3.7.8"
subtitle:   ""
date:       2018-01-30 12:00:00
author:     "FTS Team"
tags:       [CORE, CANDIDATE]

---
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1042'>FTS-1042</a>] -         t_credential.termination_time should not have the &quot;ON UPDATE&quot; clause
</li>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1054'>FTS-1054</a>] -         Inconsistent capitalisation requirements in REST and clients
</li>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1074'>FTS-1074</a>] -         nostreams by default should be 1
</li>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1109'>FTS-1109</a>] -         When the backup process starts, transfers get scheduled even if Drain is enabled 
</li>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1111'>FTS-1111</a>] -         WebMon: Y axis for success rate should always be scaled to 100%
</li>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1116'>FTS-1116</a>] -         TransferService sometimes do not generate a warning if run out of url copy slots
</li>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1119'>FTS-1119</a>] -         FTS server should depend on gfal2 &gt;= 2.14.2
</li>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1134'>FTS-1134</a>] -         FTS service and bringonline unit files should depend on network-online
</li>
</ul>
                
<h2>        Task
</h2>
<ul>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1008'>FTS-1008</a>] -         Publish final IPs on the transfer completion message
</li>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1094'>FTS-1094</a>] -         getActivePairs in OptimizerDataSource should return ordered results
</li>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1105'>FTS-1105</a>] -         Provide EMA information in Optimizer chapter
</li>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1120'>FTS-1120</a>] -         Move refresh interval to 1 hour
</li>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1129'>FTS-1129</a>] -         Add link to Grafana in webmon with the source and destination filters 
</li>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1136'>FTS-1136</a>] -         Add new logo to webmon
</li>
</ul>
    
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1093'>FTS-1093</a>] -         Display explicitly time zones
</li>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1118'>FTS-1118</a>] -         Add an option to disable table backups
</li>
</ul>
                                        
<h2>        User Documentation
</h2>
<ul>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1104'>FTS-1104</a>] -         MaxUrlCopyProcesses default as documented is wrong
</li>
</ul>
                                                       