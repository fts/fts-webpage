---
layout:     post
title:      "FTS 3.9.2"
subtitle:   ""
date:       2019-09-02 12:00:00
author:     "FTS Team"
tags:       [CORE, PRODUCTION]

---
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1455'>FTS-1455</a>] -         User DN is published for state change transfer by REST
</li>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1457'>FTS-1457</a>] -         Wrong parameter in db upgrade script
</li>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1459'>FTS-1459</a>] -         Remove any gsoap reference in cmake/spec file
</li>
</ul>
            
<h2>        Task
</h2>
<ul>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1453'>FTS-1453</a>] -         Link statically FTS CLI to the latest curl 
</li>
</ul>
    
<h2>        Improvement
</h2>
<ul>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1462'>FTS-1462</a>] -         Add a parameter to /fts3/ftsmon/overview to return only the summary of the transfers
</li>
</ul>
