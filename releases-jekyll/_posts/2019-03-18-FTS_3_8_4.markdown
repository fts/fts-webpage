---
layout:     post
title:      "FTS 3.8.4"
subtitle:   ""
date:       2019-03-18 12:00:00
author:     "FTS Team"
tags:       [CORE, PRODUCTION]

---
<h2>        Bug
</h2>
<ul>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1381'>FTS-1381</a>] -         FTS fails to build on rawhide
</li>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1392'>FTS-1392</a>] -         Stuck Session Reuse transfers in case of automatic retries  
</li>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1395'>FTS-1395</a>] -         Allow checksums identifier in C++ Client for backwards compatibility
</li>
<li>[<a href='https://its.cern.ch/jira/browse/FTS-1389'>FTS-1389</a>] -         Covert session reuse job  to Normal if the number of files is less than a configurable threshold
</li>
</ul>
                                                                                                                                                                                                                    

