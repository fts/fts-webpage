---
layout:     post
title:      "WebFTS 2.2.3"
subtitle:   ""
date:       2015-06-24 12:00:00
author:     "FTS Team"
tags:       [WebFTS, PRODUCTION]

---
<h2>Bug</h2>

<ul>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-264">FTS-264</a>] - Wrong file Attributes displayed for Grid SE endpoints</li>
</ul>