---
layout:     post
title:      "WebFTS 2.2.1"
subtitle:   ""
date:       2015-02-12 12:00:00
author:     "FTS Team"
tags:       [WebFTS, PRODUCTION]

---
<h2>Bug</h2>

<ul>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-189">FTS-189</a>] - Dropbox URL with spaces are not working</li>
</ul>

<h2>Task</h2>

<ul>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-193">FTS-193</a>] - Change Transfer sent label</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-194">FTS-194</a>] - Reload Dropbox files when already logged in</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-196">FTS-196</a>] - Add a way to remove the dropbox OAUTH credentials</li>
	<li>[<a href="https://its.cern.ch/jira/browse/FTS-202">FTS-202</a>] - Use sha512 in WebFTS</li>
</ul>